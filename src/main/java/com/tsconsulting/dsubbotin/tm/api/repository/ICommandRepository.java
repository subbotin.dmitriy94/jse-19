package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;

import java.util.Collection;

public interface ICommandRepository {

    AbstractCommand getCommandByName(final String name);

    AbstractCommand getCommandByArg(final String arg);

    Collection<AbstractCommand> getCommands();

    Collection<AbstractSystemCommand> getArguments();

    Collection<String> getCommandNames();

    Collection<String> getCommandArgs();

    void add(final AbstractCommand command);

}
