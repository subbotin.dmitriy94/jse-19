package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.AbstractEntity;

import java.util.Comparator;
import java.util.List;

public interface IRepository<E extends AbstractEntity> {

    void add(final E entity) throws AbstractException;

    void remove(final E entity) throws AbstractException;

    void clear();

    List<E> findAll();

    List<E> findAll(final Comparator<E> comparator);

    E findById(final String id) throws AbstractException;

    E findByIndex(final int index) throws AbstractException;

    E removeById(final String id) throws AbstractException;

    E removeByIndex(final int index) throws AbstractException;

}