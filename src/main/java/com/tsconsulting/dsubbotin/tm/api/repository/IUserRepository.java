package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    User findByLogin(final String login) throws AbstractException;

    User removeByLogin(final String login) throws AbstractException;

    boolean isLogin(final String login);

}
