package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.List;

public interface ITaskRepository extends IRepository<Task> {

    boolean existById(final String id) throws AbstractException;

    Task findByName(final String name) throws AbstractException;

    void removeByName(final String name) throws AbstractException;

    void updateBuyId(final String id, final String name, final String description) throws AbstractException;

    void updateBuyIndex(final int index, final String name, final String description) throws AbstractException;

    void startById(final String id) throws AbstractException;

    void startByIndex(final int index) throws AbstractException;

    void startByName(final String name) throws AbstractException;

    void finishById(final String id) throws AbstractException;

    void finishByIndex(final int index) throws AbstractException;

    void finishByName(final String name) throws AbstractException;

    void updateStatusById(final String id, final Status status) throws AbstractException;

    void updateStatusByIndex(final int index, final Status status) throws AbstractException;

    void updateStatusByName(final String name, final Status status) throws AbstractException;

    void bindTaskToProjectById(final String projectId, final String taskId) throws AbstractException;

    void unbindTaskById(final String id) throws AbstractException;

    List<Task> findAllByProjectId(final String id) throws AbstractException;

    void removeAllTaskByProjectId(final String id);

}
