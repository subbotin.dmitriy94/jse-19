package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    void bindTaskToProject(final String projectId, final String taskId) throws AbstractException;

    void unbindTaskFromProject(final String projectId, final String taskId) throws AbstractException;

    List<Task> findAllTasksByProjectId(final String id) throws AbstractException;

    void removeProjectById(final String id) throws AbstractException;

    void removeProjectByIndex(final int index) throws AbstractException;

    void removeProjectByName(final String name) throws AbstractException;

}
