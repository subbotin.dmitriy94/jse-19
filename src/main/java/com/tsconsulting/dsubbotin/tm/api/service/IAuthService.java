package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;

public interface IAuthService {

    String getCurrentUserId() throws AbstractException;

    void setCurrentUserId(final String id) throws AbstractException;

    boolean isAuth() throws AbstractException;

    void logout() throws AbstractException;

    void login(final String login, final String password) throws AbstractException;

    void registry(final String login,
                  final String password,
                  final String email) throws AbstractException;

    User getUser() throws AbstractException;


}
