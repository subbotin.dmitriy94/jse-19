package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;

public interface IUserService extends IService<User> {

    void create(final String login, final String password) throws AbstractException;

    void create(final String login,
                final String password,
                final Role role) throws AbstractException;

    void create(final String login,
                final String password,
                final Role role,
                final String email) throws AbstractException;

    User findByLogin(final String login) throws AbstractException;

    User removeByLogin(final String login) throws AbstractException;

    void setPassword(final String id, final String password) throws AbstractException;

    User setRole(final String id, final Role role) throws AbstractException;

    User updateById(final String id,
                    final String lastName,
                    final String firstName,
                    final String middleName,
                    final String email) throws AbstractException;

    boolean isLogin(final String login) throws AbstractException;

}
