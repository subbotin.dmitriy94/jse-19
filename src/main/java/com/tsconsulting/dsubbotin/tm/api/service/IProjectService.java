package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Project;

public interface IProjectService extends IService<Project> {

    void create(final String name) throws AbstractException;

    void create(final String name, final String description) throws AbstractException;

    Project findByName(final String name) throws AbstractException;

    void updateById(final String id, final String name, final String description) throws AbstractException;

    void updateByIndex(final int index, final String name, final String description) throws AbstractException;

    void startById(final String id) throws AbstractException;

    void startByIndex(final int index) throws AbstractException;

    void startByName(final String name) throws AbstractException;

    void finishById(final String id) throws AbstractException;

    void finishByIndex(final int index) throws AbstractException;

    void finishByName(final String name) throws AbstractException;

    void updateStatusById(final String id, final Status status) throws AbstractException;

    void updateStatusByIndex(final int index, final Status status) throws AbstractException;

    void updateStatusByName(final String name, final Status status) throws AbstractException;

}
