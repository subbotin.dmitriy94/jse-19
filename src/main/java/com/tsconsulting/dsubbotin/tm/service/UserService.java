package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IUserService;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyLoginException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyPasswordException;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;

public final class UserService extends AbstractService<User> implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        super(userRepository);
        this.userRepository = userRepository;
    }

    @Override
    public void create(final String login, final String password) throws AbstractException {
        checkLogin(login);
        checkPassword(password);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        userRepository.add(user);
    }

    @Override
    public void create(final String login,
                       final String password,
                       final Role role) throws AbstractException {
        checkLogin(login);
        checkPassword(password);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        userRepository.add(user);
    }

    @Override
    public void create(final String login,
                       final String password,
                       final Role role,
                       final String email) throws AbstractException {
        checkLogin(login);
        checkPassword(password);
        final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        user.setRole(role);
        user.setEmail(email);
        userRepository.add(user);
    }

    @Override
    public User findByLogin(final String login) throws AbstractException {
        checkLogin(login);
        return userRepository.findByLogin(login);
    }

    @Override
    public User removeByLogin(final String login) throws AbstractException {
        checkLogin(login);
        return userRepository.removeByLogin(login);
    }

    @Override
    public void setPassword(final String id, final String password) throws AbstractException {
        checkId(id);
        checkPassword(password);
        final User user = findById(id);
        user.setPasswordHash(HashUtil.salt(password));
    }

    @Override
    public User setRole(final String id, final Role role) throws AbstractException {
        checkId(id);
        final User user = findById(id);
        user.setRole(role);
        return user;
    }

    @Override
    public User updateById(final String id,
                           final String lastName,
                           final String firstName,
                           final String middleName,
                           final String email) throws AbstractException {
        checkId(id);
        final User user = findById(id);
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setEmail(email);
        return user;
    }

    @Override
    public boolean isLogin(final String login) throws AbstractException {
        checkLogin(login);
        return userRepository.isLogin(login);
    }

    private void checkLogin(String login) throws EmptyLoginException {
        if (EmptyUtil.isEmpty(login)) throw new EmptyLoginException();
    }

    private void checkId(String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkPassword(String password) throws EmptyPasswordException {
        if (EmptyUtil.isEmpty(password)) throw new EmptyPasswordException();
    }

}