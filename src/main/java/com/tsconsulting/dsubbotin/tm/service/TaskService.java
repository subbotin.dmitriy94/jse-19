package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.ITaskService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;

public final class TaskService extends AbstractService<Task> implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(final String name) throws AbstractException {
        checkName(name);
        Task task = new Task();
        task.setName(name);
        taskRepository.add(task);
    }

    @Override
    public void create(final String name,
                       final String description) throws AbstractException {
        checkName(name);
        if (EmptyUtil.isEmpty(description)) throw new EmptyDescriptionException();
        Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(task);
    }

    @Override
    public Task findByIndex(final Integer index) throws AbstractException {
        checkIndex(index);
        return taskRepository.findByIndex(index);
    }

    @Override
    public Task findByName(final String name) throws AbstractException {
        checkName(name);
        return taskRepository.findByName(name);
    }

    @Override
    public void removeByName(final String name) throws AbstractException {
        checkName(name);
        taskRepository.removeByName(name);
    }

    @Override
    public void updateById(final String id,
                           final String name,
                           final String description) throws AbstractException {
        checkId(id);
        checkName(name);
        taskRepository.updateBuyId(id, name, description);
    }

    @Override
    public void updateByIndex(final int index,
                              final String name,
                              final String description) throws AbstractException {
        checkIndex(index);
        checkName(name);
        taskRepository.updateBuyIndex(index, name, description);
    }

    @Override
    public void startById(final String id) throws AbstractException {
        checkId(id);
        taskRepository.startById(id);
    }

    @Override
    public void startByIndex(final int index) throws AbstractException {
        checkIndex(index);
        taskRepository.startByIndex(index);
    }

    @Override
    public void startByName(final String name) throws AbstractException {
        checkName(name);
        taskRepository.startByName(name);
    }

    @Override
    public void finishById(final String id) throws AbstractException {
        checkId(id);
        taskRepository.finishById(id);
    }

    @Override
    public void finishByIndex(final int index) throws AbstractException {
        checkIndex(index);
        taskRepository.finishByIndex(index);
    }

    @Override
    public void finishByName(final String name) throws AbstractException {
        checkName(name);
        taskRepository.finishByName(name);
    }

    @Override
    public void updateStatusById(final String id,
                                 final Status status) throws AbstractException {
        checkId(id);
        taskRepository.updateStatusById(id, status);
    }

    @Override
    public void updateStatusByIndex(final int index,
                                    final Status status) throws AbstractException {
        checkIndex(index);
        taskRepository.updateStatusByIndex(index, status);
    }

    @Override
    public void updateStatusByName(final String name,
                                   final Status status) throws AbstractException {
        checkName(name);
        taskRepository.updateStatusByName(name, status);
    }

    private void checkName(String name) throws EmptyNameException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
    }

    private void checkId(String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkIndex(Integer index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
    }

}