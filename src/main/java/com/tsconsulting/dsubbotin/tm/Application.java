package com.tsconsulting.dsubbotin.tm;

import com.tsconsulting.dsubbotin.tm.component.Bootstrap;

public final class Application {

    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}