package com.tsconsulting.dsubbotin.tm.command.user;

import com.tsconsulting.dsubbotin.tm.command.AbstractUserCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-registry";
    }

    @Override
    public String description() {
        return "User registry.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Entr login:");
        final String login = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Entr password:");
        final String password = TerminalUtil.nextLine();
        TerminalUtil.printMessage("Entr email:");
        final String email = TerminalUtil.nextLine();
        serviceLocator.getAuthService().registry(login, password, email);
        TerminalUtil.printMessage("User registered:");
    }

}
