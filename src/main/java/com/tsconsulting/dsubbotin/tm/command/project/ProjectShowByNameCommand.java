package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class ProjectShowByNameCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-show-by-name";
    }

    @Override
    public String description() {
        return "Display project by name.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter name:");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().findByName(name);
        showProject(project);
    }

}
