package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownStatusException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectUpdateStatusByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-update-status-by-id";
    }

    @Override
    public String description() {
        return "Update status project by id.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter id:");
        final String id = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findById(id);
        TerminalUtil.printMessage("Enter status:");
        TerminalUtil.printMessage(Arrays.toString(Status.values()));
        final String statusValue = TerminalUtil.nextLine();
        try {
            Status status = Status.valueOf(statusValue);
            serviceLocator.getProjectService().updateStatusById(id, status);
            TerminalUtil.printMessage("[Updated project status]");
        } catch (IllegalArgumentException e) {
            throw new UnknownStatusException();
        }
    }

}
