package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String name() {
        return "project-remove-by-id";
    }

    @Override
    public String description() {
        return "Remove project by id.";
    }

    @Override
    public void execute() throws AbstractException {
        TerminalUtil.printMessage("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        serviceLocator.getProjectService().findById(projectId);
        serviceLocator.getProjectTaskService().removeProjectById(projectId);
        TerminalUtil.printMessage("[Project removed]");
    }

}
