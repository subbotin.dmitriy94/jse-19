package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IRepository;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.EntityNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> entities = new ArrayList<>();

    @Override
    public void add(final E entity) {
        entities.add(entity);
    }

    @Override
    public void remove(final E entity) {
        entities.remove(entity);
    }

    @Override
    public void clear() {
        entities.clear();
    }

    @Override
    public List<E> findAll() {
        return entities;
    }

    public List<E> findAll(final Comparator<E> comparator) {
        final List<E> entitiesList = new ArrayList<>(entities);
        entitiesList.sort(comparator);
        return entitiesList;
    }

    @Override
    public E findById(final String id) throws AbstractException {
        for (E entity : entities) {
            if (id.equals(entity.getId())) return entity;
        }
        throw new EntityNotFoundException();
    }

    @Override
    public E findByIndex(final int index) throws AbstractException {
        if (entities.size() - 1 < index) throw new IndexIncorrectException();
        return entities.get(index);
    }

    @Override
    public E removeById(final String id) throws AbstractException {
        final E entity = findById(id);
        entities.remove(entity);
        return entity;
    }

    @Override
    public E removeByIndex(final int index) throws AbstractException {
        final E entity = findByIndex(index);
        entities.remove(entity);
        return entity;
    }

}