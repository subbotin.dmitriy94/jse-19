package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.UserNotFoundException;
import com.tsconsulting.dsubbotin.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) throws AbstractException {
        for (User user : entities) {
            if (login.toLowerCase().equals(user.getLogin().toLowerCase())) return user;
        }
        throw new UserNotFoundException();
    }

    @Override
    public User removeByLogin(final String login) throws AbstractException {
        final User user = findByLogin(login);
        entities.remove(user);
        return user;
    }

    @Override
    public boolean isLogin(final String login) {
        for (User user : entities) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

}
