package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.ProjectNotFoundException;
import com.tsconsulting.dsubbotin.tm.model.Project;

import java.util.Date;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {

    @Override
    public boolean existById(final String id) throws AbstractException {
        return findById(id) != null;
    }

    @Override
    public Project findByName(final String name) throws AbstractException {
        for (Project project : entities) {
            if (name.equals(project.getName())) return project;
        }
        throw new ProjectNotFoundException();
    }

    @Override
    public void removeByName(final String name) throws AbstractException {
        final Project project = findByName(name);
        entities.remove(project);
    }

    @Override
    public void updateBuyId(final String id,
                            final String name,
                            final String description) throws AbstractException {
        final Project project = findById(id);
        project.setName(name);
        project.setDescription(description);
    }

    @Override
    public void updateByIndex(final int index,
                              final String name,
                              final String description) throws AbstractException {
        final Project project = findByIndex(index);
        project.setName(name);
        project.setDescription(description);
    }

    @Override
    public void startById(final String id) throws AbstractException {
        final Project project = findById(id);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
    }

    @Override
    public void startByIndex(final int index) throws AbstractException {
        final Project project = findByIndex(index);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
    }

    @Override
    public void startByName(final String name) throws AbstractException {
        final Project project = findByName(name);
        project.setStatus(Status.IN_PROGRESS);
        project.setStartDate(new Date());
    }

    @Override
    public void finishById(final String id) throws AbstractException {
        final Project project = findById(id);
        project.setStatus(Status.COMPLETED);
    }

    @Override
    public void finishByIndex(final int index) throws AbstractException {
        final Project project = findByIndex(index);
        project.setStatus(Status.COMPLETED);
    }

    @Override
    public void finishByName(final String name) throws AbstractException {
        final Project project = findByName(name);
        project.setStatus(Status.COMPLETED);
    }

    @Override
    public void updateStatusById(final String id,
                                 final Status status) throws AbstractException {
        final Project project = findById(id);
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
    }

    @Override
    public void updateStatusByIndex(final int index,
                                    final Status status) throws AbstractException {
        final Project project = findByIndex(index);
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
    }

    @Override
    public void updateStatusByName(final String name,
                                   final Status status) throws AbstractException {
        final Project project = findByName(name);
        project.setStatus(status);
        if (status == Status.IN_PROGRESS) project.setStartDate(new Date());
    }

}